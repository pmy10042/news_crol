			  IEC Documentation
			---------------------

Copyright (c) 2004, Mayukh Bose
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

* Neither the name of Mayukh Bose nor the names of other contributors
may be used to endorse or promote products derived from this software
without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

============================================================================

			    Class Methods
			    -------------


__init__(self, window_num=0, window_url=''):

Creates a new Internet Explorer Window, if window_num = 0 and
window_url = '' . If window_num > 0, the class looks at the list of
open Internet Explorer window, and attempts to hook to the one
corresponding to window_num. If window_url is specified, the code
looks at the list of open Internet Explorer windows and attempts to
find one where the URL contains the string specified. If no such
window can be found, the code opens a new Internet Explorer window.

USAGE:
import IEC

ie = IEC.IEController()  # Creates a new IE Window
ie = IEC.IEController(window_num=0) # Same as above
ie = IEC.IEController(2) # Hooks to the 2nd IE Window open
ie = IEC.IEController(window_num2) # Hooks to the 2nd IE window open
ie = IEC.IEController(window_url='google') # Finds the window that has
                                           # 'google' in the url.

----------------------------------------------------------------------------


CloseWindow(self):

Closes the Internet Explorer window.

USAGE:
import IEC

ie = IEC.IEController() # Creates a new IE Window
ie.CloseWindow()        # Closes the window

----------------------------------------------------------------------------


PollWhileBusy(self):

This method is mostly used by other methods in the class, but is made
public, in case a user needs it. This method polls the Internet
Explorer instance and returns if IE is NOT busy doing anything. 

USAGE:
import IEC

ie = IEC.IEController()  # Creates a new IE Window
# ... do something here to make the IE instance busy
ie.PollWhileBusy()       # Wait till IE is done processing.
# ... do something else

----------------------------------------------------------------------------


Navigate(self, url):

Navigates to the specified URL. The method calls PollWhileBusy()
internally and returns when it has finished navigating to the URL.

USAGE:
import IEC

ie = IEC.IEController()                # Creates a new IE Window
ie.Navigate('http://www.google.com/')  # Navigate to a website.

----------------------------------------------------------------------------


GetDocumentHTML(self):

Returns the HTML code of the <BODY> of the document as a string. This
is useful to examine the actual page source and grab certain data from
it. Note that the document returned has the tags normalized by IE and
may not coincide with the page source. This means, if the actual page
source has something like this: <a Href="http://www.foo.com/">Foo</a>,
the returned source will say: <A href="http://www.foo.com/">Foo</A>.
This is because IE internally normalizes the document so that each
HTML tag is in capital letters and all the attributes are lowercased.
Note that only the HTML tags are converted. The text between the tags
is left alone.

USAGE:
import IEC

ie = IEC.IEController()                # Creates a new IE Window
ie.Navigate('http://www.google.com/')  # Navigate to a website.
html = ie.GetDocumentHTML()            # Get the HTML

----------------------------------------------------------------------------


GetDocumentText(self):

Returns the body text of the document as a string. This is similar to
the previous method, but this one removes all the HTML tags and only
returns the document's text.

USAGE:
import IEC

ie = IEC.IEController()                # Creates a new IE Window
ie.Navigate('http://www.google.com/')  # Navigate to a website.
text = ie.GetDocumentText()            # Get the text

----------------------------------------------------------------------------


GetCurrentUrl(self):

Returns the current URL that the browser is on. This is handy to have
because navigating to some URLs results in the browser getting a
redirect message to another URL. This function returns the final URL
after the redirect has been processed.

USAGE:
import IEC

ie = IEC.IEController()                # Creates a new IE Window
ie.Navigate('http://www.google.com/')  # Navigate to a website.
url = ie.GetCurrentUrl()               # Returns the final URL.

----------------------------------------------------------------------------


ClickLink(self, linktext):

Clicks on an HTML link that matches linktext. You can use this method
to automatically click on links. The matching is case insensitive. The
method calls PollWhileBusy internally and returns after navigation is 
complete.

USAGE:
import IEC

ie = IEC.IEController()                # Creates a new IE Window
ie.Navigate('http://www.google.com/')  # Navigate to a website.
ie.ClickLink('about GOOGLE')           # Click the "About Google" link
                                       # Note case-insensitivity.
----------------------------------------------------------------------------


ClickButton(self, name='', caption=''):

Clicks on a button. You may specify either the name, or the caption,
and the code will attempt to find a matching button and click it. The
method calls PollWhileBusy() internally and returns after navigation
is complete. Note that the matching is case-insensitive.

USAGE:
import IEC

ie = IEC.IEController()                # Creates a new IE Window
ie.Navigate('http://www.google.com/')  # Navigate to a website.
ie.ClickButton(name='btnG')            # Click on a button named btnG
ie.ClickButton(caption='Google Search') # Click on "Google Search"

----------------------------------------------------------------------------


GetInputValue(self, name):

Returns the value of an HTML edit box field, whose name matches the
passed in parameter. The matching is case-insensitive.

USAGE:
import IEC

ie = IEC.IEController()                # Creates a new IE Window
ie.Navigate('http://www.google.com/')  # Navigate to a website.
# Get the text from the edit box named 'q'.
str = ie.GetInputValue('q')

----------------------------------------------------------------------------


SetInputValue(self, name):

Sets the value of an HTML edit box field, whose name matches the
passed in parameter. The matching is case-insensitive.

USAGE:
import IEC

ie = IEC.IEController()                # Creates a new IE Window
ie.Navigate('http://www.google.com/')  # Navigate to a website.
# Set the text for the edit box named 'q'.
ie.SetInputValue('q', 'mayukh bose')

----------------------------------------------------------------------------

GetTextArea(self, name):

Returns the value of an HTML text area field, whose name matches the
passed in parameter. The matching is case-insensitive.

USAGE:
import IEC

ie = IEC.IEController()                # Creates a new IE Window
ie.Navigate('http://www.example.com/') # Navigate to some site.
# Get the text from a <TEXTAREA> named 'memo'.
str = ie.GetInputValue('memo')

----------------------------------------------------------------------------


SetTextArea(self, name, value):

Sets the value of an HTML text area field, whose name matches the
passed in parameter. The matching is case-insensitive.

USAGE:
import IEC

ie = IEC.IEController()                # Creates a new IE Window
ie.Navigate('http://www.example.com/') # Navigate to some site.
# Sets the text for a <TEXTAREA> named 'memo'.
str = ie.GetInputValue('memo', 'This is a\nmultiple line\nmemo')

----------------------------------------------------------------------------


GetSelectValue(self, name):

Returns the value of an HTML SELECT field, whose name matches the
passed in parameter. The matching is case-insensitive. The return
value is a tuple consisting of the currently selected element value
and the element text.

USAGE:
import IEC

# Create a new IE Window
ie = IEC.IEController()
# Navigate to a website
ie.Navigate('http://www.google.com/preferences')  
# Get the text from an HTML SELECT named 'hl'.
(value, text) = ie.GetSelectValue('hl')

----------------------------------------------------------------------------


SetSelectValue(self, selname, optionvalue='', optioncaption=''):

Sets the value of an HTML SELECT field, whose name matches the
passed in parameter. The matching is case-insensitive. You can pass
either the option value, or the option's text (caption) as the new
value and the method will set it accordingly.

USAGE:
import IEC

# Create a new IE Window
ie = IEC.IEController()
# Navigate to a website
ie.Navigate('http://www.google.com/preferences')  
# Set the language selection (SELECT=hl) to xx-elmer(Elmer Fudd)
# <OPTION value=xx-elmer>Elmer Fudd</OPTION>
ie.SetSelectValue('hl', optionvalue='xx-elmer')
# Set the language selection (SELECT=hl) to "Hacker"
# <OPTION value=xx-hacker>Hacker </OPTION>
ie.SetSelectValue('hl', optioncaption='hacker')

----------------------------------------------------------------------------


GetListSelection(selname):

Gets the list of currently selected items from a HTML SELECT field
with the multi-select option enabled. The name of the select element
should match selname. The matching is case-insensitive. The return
value is a list of tuples corresponding to the currently selected
items in the list. Each tuple has the option value and the option text
in it.

USAGE:
import IEC

ie = IEC.IEController()               # Create a new IE Window
ie.Navigate('http://www.example.com') # Navigate to a website
# Now get the list of selected items from a multi-select element
items = ie.GetListSelection('multi')
for item in items:
    (optionvalue, optiontext) = item

----------------------------------------------------------------------------


SetAllListElements(self, selname, selected = 0):

Sets all the elements of a multi-select HTML SELECT field to a given
value. By default, the selected parameter is 0 (i.e.) unselect all. If
the value of selected is set to 1, then the method selects all the
elements. As for all other methods, matching for selname is case
insensitive. 

USAGE:
import IEC

ie = IEC.IEController()               # Create a new IE Window
ie.Navigate('http://www.example.com') # Navigate to a website
# Unselect all the elements of a multi-select element
items = ie.SetAllListElements('multi1')
# Select all the elements of a multi-select element
items = ie.SetAllListElements('multi2', 1)

----------------------------------------------------------------------------


SetListSelection(self, selname, optionvalue = '', optioncaption = '', 
		       selected = 1):

Selects or unselects the specified element of a multi-select HTML
SELECT field. If selected = 1, then the element is selected. If
selected = 0, then the element is unselected. You can specify either
the option value, or the option text (caption) to be
selected. Matching for selname, optionvalue and optioncaption is
case-insensitive. 

USAGE:
import IEC

ie = IEC.IEController()               # Create a new IE Window
ie.Navigate('http://www.example.com') # Navigate to a website
# Assume the page has a multi-select like this:
# <SELECT name="multi1">
#   <OPTION name="opt1">Option 1</OPTION>
#   <OPTION name="opt2" selected>Option 2</OPTION>
#   <OPTION name="opt3">Option 3</OPTION>
#   <OPTION name="opt4">Option 4</OPTION>
# </SELECT>
# Select the first option by element value.
ie.SetListSelection('multi1', optionvalue="opt1")
# Unselect the second element
ie.SetListSelection('multi1', optionvalue="opt2", 0)
# Select the third option by element name.
ie.SetListSelection('multi1', optioncaption="Option 3")

----------------------------------------------------------------------------


GetCheckBoxState(self, cbname):

Finds the checkbox named cbname and returns its state. The matching
for the name is case-insensitive. The return value is either 1 (for
checked) or 0 (unchecked).

USAGE:
import IEC

# Create a new IE Window
ie = IEC.IEController()
# Navigate to a website
ie.Navigate('http://www.google.com/preferences')
# Now get the state of the checkbox for Dutch
# <INPUT name=lr type=checkbox value=lang_nl> Dutch
nl_checked = GetCheckBoxState('lang_nl')

----------------------------------------------------------------------------


SetCheckBoxState(self, cbname, checked = 1):

Finds the checkbox named cbname and sets its state. The matching
for the name is case-insensitive. Passing checked = 1 (the default)
checks the box. Passing checked = 0 unchecks the box.

USAGE:
import IEC

# Create a new IE Window
ie = IEC.IEController()
# Navigate to a website
ie.Navigate('http://www.google.com/preferences')
# Now set the state of the checkbox for Dutch
# <INPUT name=lr type=checkbox value=lang_nl> Dutch
SetCheckBoxState('lang_nl')    # Default is to check
SetCheckBoxState('lang_nl', 0) # uncheck the box.

----------------------------------------------------------------------------


GetRadioValue(self, rbname):

Finds the radio group named rbname and returns its value. The matching
for the name is case-insensitive. The return value is the value of the
currently selected radio group option.

USAGE:
import IEC

# Create a new IE Window
ie = IEC.IEController()
# Navigate to a website
ie.Navigate('http://www.example.com/')
# Assume the page has a radio group like this:
# <input type="radio" name="genre" value="Metal" checked>Metal<br>
# <input type="radio" name="genre" value="Punk">Punk<br>
# <input type="radio" name="genre" value="Classical">Classical<br>
genre = GetRadioValue('genre')      # should be set to "Metal"

----------------------------------------------------------------------------


SetRadioValue(self, rbname, rbvalue, checked = 1):

Finds the radio group named rbname and sets its value. The matching
for the name is case-insensitive. Passing checked = 1 (the default)
checks the radiobutton. Passing checked = 0 unchecks the radiobutton.

USAGE:
import IEC

ie = IEC.IEController()               # Create a new IE Window
ie.Navigate('http://www.example.com') # Navigate to a website
# Assume the page has a radio group like this:
# <input type="radio" name="genre" value="Metal" checked>Metal<br>
# <input type="radio" name="genre" value="Punk">Punk<br>
# <input type="radio" name="genre" value="Classical">Classical<br>
SetRadioValue('genre', 'punk')        # Set the option to "Punk"

----------------------------------------------------------------------------


SubmitForm(self, formname):

Submits the form whose name is formname. The matching for the name is
case-insesitive. The form is submitted as per its METHOD (GET or
POST). This is particularly useful for those forms that have
javascript that triggers when a form element (such as a select) is
changed and forces a form post. You can simulate the same thing by
changing the element value and forcing a form post. If the form has a
submit button, you can try using ClickButton instead.

USAGE:
import IEC

ie = IEC.IEController()               # Create a new IE Window
ie.Navigate('http://www.example.com') # Navigate to a website
# Assume the page has a form and a radio group like this:
# <form name="form1" method="post" action="somepage.php">
# <input type="radio" name="genre" value="m" onchange="foo()">Metal
# <input type="radio" name="genre" value="p" onchange="foo()">Punk
# <input type="radio" name="genre" value="c: onchange="foo()">Classical
# </form>
SetRadioValue('genre', 'p')           # Set the option to "Punk"
# Now force a form post, to activate the javascript.
SubmitForm('form1')

----------------------------------------------------------------------------

