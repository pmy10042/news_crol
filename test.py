# -*- coding: utf-8 -*-  #한글을 쓸 때는 꼭 붙인다. 문자 인코딩을 UTF-8로 하겠다는 것이다. 인코딩은 앞으로 계속 속썩일 것이다.
#
import re
import sys
import win32com.client
import httplib
import json
from conf import address, sender, receivers, headers, content


reload(sys)
sys.setdefaultencoding('utf-8')


def han_test():
    name = '파일.txt'
    filename = 'html/%s' % name
    filename2 = unicode(filename)

    with open(filename2, 'w') as fp:
        fp.write('박만영바보'.encode('utf8'))

    print '한글'

    s = 'english'
    print str(unicode(s))

    s = '한글'
    print str(unicode(s))


def re_test():
    p=re.compile(".+제약.+(신약|개발)")
    m=p.match('sadf 제약 sms 신약')
    if m:
        print(m.group())
    else:
        print('not match')


def open_excel(stname,url,nowtime,contents):
    excel = win32com.client.Dispatch("Excel.Application")
    excel.Visible = True
    wb = excel.Workbooks.Add()
    ws = wb.Worksheets("Sheet1")
    ws.Cells(1, 1).Value = stname.decode('utf8')
    ws.Cells(1, 2).Value = url.decode('utf8')
    ws.Cells(1, 3).Value = nowtime.decode('utf8')
    ws.Cells(1, 4).Value = contents.decode('utf8')




def call_sms(contents):
    c = httplib.HTTPSConnection(address)

    path = "/smscenter/v1.0/sendsms"
    value = {
        'sender': sender,
        'receivers': receivers,
        'content': contents,
    }
    data = json.dumps(value, ensure_ascii=False).encode('utf-8')

    c.request("POST", path, data, headers)
    r = c.getresponse()

    print r.status, r.reason
    print r.read()



#han_test()
re_test()

open_excel("jw중외","http://www.naver.com","2016-23-23","어쩌두저쩌구")
call_sms("바보바보테스트")


