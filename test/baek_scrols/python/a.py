import requests
from bs4 import BeautifulSoup
import os
import hashlib
import time
import shutil

url = 'http://news.naver.com/main/ranking/popularDay.nhn'

def download_url(link, file_name):
    if not os.path.exists('html'):
        os.makedirs('html')
    file_name = 'html/' + file_name
    if os.path.exists(file_name):
        with open(file_name,'r') as fp:
            source = fp.read()
    else:
        time.sleep(1)
        r = requests.get(link)
        with open(file_name,'w') as fp:
            fp.write(r.text)
        source = r.text
    return source

source = download_url(url, 'news.html')

soup = BeautifulSoup(source, 'html.parser')

div = soup.find('div','content')
sections = div.find_all('div','ranking_section')

print len(sections)

def download_image(img_url):
    if not os.path.exists('image'):
        os.makedirs('image')
    print 'image url:',img_url
    file_name = img_url.split('/')[-1]
    file_name = file_name.split('?')[0]
    print 'file name:',file_name
    file_name = 'image/' + file_name
    if os.path.exists(file_name):
        return
    r = requests.get(img_url, stream=True)
    with open(file_name,'wb') as fp:
        r.raw.decode_content = True
        shutil.copyfileobj(r.raw, fp)

def find_image(news_link):
    if news_link[:4] != 'http':
        news_link = 'http://news.naver.com' + news_link
    print news_link
    m = hashlib.md5()
    m.update(news_link)
    print m.hexdigest()
    source = download_url(news_link, m.hexdigest()+'.html')
    soup = BeautifulSoup(source, 'html.parser')
    article_body = soup.find('div',id='articleBodyContents')
    if not article_body:
        article_body = soup.find('div',id='articeBody')
    if not article_body:
        article_body = soup.find('div',id='newsEndContents')
    if not article_body:
        if soup.script and soup.script.text.startswith(u'document.location.replace'):
            temp = soup.script.text.strip()
            temp = temp.lstrip('document.location.replace(')
            temp = temp[:-2]
            temp = temp.strip("'")
            print 'replace url:',temp
            find_image(temp)
            return
    img = article_body.find('img')
    if img:
        img_url = img['src']
        download_image(img_url)

for section in sections:
    section_title = section.h4.em.string
    items = section.ol.find_all('li')
    #print section_title, len(items)
    for item in items:
        if item.dl:
            news_title = item.dl.dt.a.string.strip()
            news_link = item.dl.dt.a['href']
        else:
            news_title = item.p.a.string.strip()
            news_link = item.p.a['href']
        #print news_title
        #print news_link
        find_image(news_link)




