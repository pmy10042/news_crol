# -*- encoding: utf-8 -*-
import os
import requests
from bs4 import BeautifulSoup

url = 'http://dart.fss.or.kr/dsab001/search.ax'

def check_end(file_name):
    with open(file_name,'r') as fp:
        soup = BeautifulSoup(fp.read(),'html.parser')
    td = soup.table.tbody.tr.td
    if td.get('colspan') is not None:
        return True
    else:
        return False

def download_page(company_name, company_code, page):
    print company_name, page
    data = {
        'currentPage': page,
        'maxResults':  '15',
        'maxLinks':    '10',
        'sort':    'date',
        'series':  'desc',
        'textCrpCik':  company_code,
        'textCrpNm':   company_name,
        'finalReport': 'recent',
        'startDate':   '20151002',
        'endDate': '20160402'
    }
    file_name = 'html/%s/%d.html'%(company_code,page)
    if not os.path.exists(file_name):
        r = requests.post(url, data=data)
        if not os.path.exists('html/%s'%(company_code)):
            os.mkdir('html/%s'%(company_code))
        with open(file_name,'w') as fp:
            fp.write(r.text)
    return check_end(file_name)

for i in range(1,100000):
    last = download_page(u'삼성전자', '00126380', i)
    if last:
        break
