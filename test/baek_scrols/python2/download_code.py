import time
import os
import requests
from bs4 import BeautifulSoup
url = 'http://dart.fss.or.kr/corp/searchCorpL.ax'

for i in range(1, 188):
    print 'current page:',i
    file_name = 'list/%d.html'%i
    if os.path.exists(file_name):
        continue
    r = requests.post(url,{'currentPage': i})
    with open(file_name,'w') as fp:
        fp.write(r.text)
    time.sleep(0.2)
