from bs4 import BeautifulSoup
import json

def read_page(page):
    file_name = 'list/%d.html'%page
    with open(file_name,'r') as fp:
        soup = BeautifulSoup(fp.read(),'html.parser')
    trs = soup.find('div','table_scroll').table.tbody.find_all('tr')
    companies = []
    for tr in trs:
        tds = tr.find_all('td')
        inputs = tds[0].find_all('input')
        for x in inputs:
            if x['name'] == 'hiddenCikCD1':
                company_code = x['value']
            elif x['name'] == 'hiddenCikNM1':
                company_name = x['value']
        if tds[1].string:
            company_ceo = tds[1].string.strip()
        else:
            company_ceo = ''
        company_category = tds[3].string.strip() if tds[3].string else ''
        #print company_name, company_code, company_ceo, company_category
        companies.append({
            'name': company_name,
            'code': company_code,
            'ceo': company_ceo,
            'category': company_category
        })
    return companies
total_page = 188
ans = []
for page in range(1,total_page):
    print page
    ans += read_page(page)

print len(ans)

with open('all.json','w') as fp:
    fp.write(json.dumps(ans))
