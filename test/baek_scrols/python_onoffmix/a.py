import requests

url = 'http://onoffmix.com/'

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36'
}

login_url = 'https://onoffmix.com:443/account/login'

data = {
    'proc': 'login',
    'returnUrl': 'http://onoffmix.com/',
    'email': 'onoffmix@gmail.com',
    'pw': '123123',
    'saveEmail': '1',
}

sess = requests.session()

# login
r = sess.post(login_url,data=data,headers=headers)
print r.text

# main page
r = sess.get(url, headers=headers)

with open('output.html','w') as fp:
    fp.write(r.text)

