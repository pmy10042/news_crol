# -*- coding: utf-8 -*-  #한글을 쓸 때는 꼭 붙인다. 문자 인코딩을 UTF-8로 하겠다는 것이다. 인코딩은 앞으로 계속 속썩일 것이다.

import urllib  # URL을 열고 HTML을 읽는 모듈, urllib을 불러온다
import os
import sqlite3
# http://search.daum.net/search?w=news&nil_search=btn&enc=utf8&q=영진약품&period=u&sd=20160729120000&ed=20160729235959&DA=STC

# kospi={}

import win32com.client
import time

from datetime import datetime, timedelta
import json
import IEC


def test():
    info = {'name': 'lee', 'age': 23, 'job': 'programmer'}  # 사전형

    json.dumps(info)

    info = {'name': 'lee', 'age': 23, 'job': 'programmer'}
    with open('./info.json', 'w') as f:
        json.dump(info, f)

    with open('./info.json', 'r') as f:
        l_info = json.load(f)

    for i in l_info:
        print i


def test2():
    customer = {
        'id': 152352,
        'name': '강진수',
        'history': [
            {'date': '2015-03-11', 'item': 'iPhone'},
            {'date': '2016-02-23', 'item': 'Monitor'},
        ]
    }

    # JSON 인코딩
    jsonString = json.dumps(customer)

    with open('./info2.json', 'w') as f:
        json.dump(customer, f)

    # 문자열 출력
    print(jsonString)
    print(type(jsonString))  # class str

    jsonString2 = json.dumps(customer, indent=4)
    print(jsonString2)


def test3():
    jsonString = '{"name": "강진수", "id": 152352, "history": [{"date": "2015-03-11", "item": "iPhone"}, {"date": "2016-02-23", "item": "Monitor"}]}'

    # JSON 디코딩
    dict = json.loads(jsonString)

    with open('./info2.json', 'r') as f:
        l_info = json.load(f)

    # Dictionary 데이타 체크
    print(dict['name'])

    for h in dict['history']:
        print(h['date'], h['item'])


def ariticle_save():
    article_contents = {"name": "영진약품2", 'date': '203160811123012'}
    jsonString = json.dumps(article_contents)

    with open('log.json', 'a') as f:
        json.dump(article_contents, f, indent=2)

    print '파일쓰때:', article_contents
    print '파일쓰때:', jsonString


def load_json():
    with open('log.json', 'r') as fl:
        data = json.load(fl)

    for i in data:
        print i['date'], i['name']


def tt():
    companies = []
    companies.append({
        'k1': 1234,
        'k2': 'hello'
    })

    companies.append({
        'k1': 341234,
        'k2': 'hello22'
    })

    with open('log2.json', 'w') as fp:
        json.dump(companies, fp, indent=2)


def load_tt():
    with open('log2.json', 'r') as fp:
        data = json.load(fp)

    data.append({"k33": "haaaello", "k133": 1234})

    print data

    with open('log2.json', 'w') as fp:
        json.dump(data, fp, indent=2)


def open_ie():

    ie = IEC.IEController()  # Create a new IE Window.
    nm='jw중외제약'
    nm=nm.decode('utf8')
    ie.Navigate('http://www.daum.net/')  # Navigate to a website.
    ie.SetInputValue('q', nm)  # Fill in the search box.


#    ie.ClickButton(caption='ir_wa')  # Click on the search button.

def load_data():
    with open('data.json') as f:
        data = json.load(f)

    for i in data['rows']:
        print i['date']


def sqllite_insert(st_name,url,coldate,contents):
    import sqlite3

    con = sqlite3.connect("st_tmp.db")
    print(type(con))
    cursor = con.cursor()

    try:
        rslt=cursor.execute("CREATE TABLE companyName(Name varchar(50), url text, coldate datetime, contents text )")
    except sqlite3.Error as e:
        print("이미 테이블 존재 함.error:", e.args[0])
    else:
        print '테이블 생성 완료'


    st_name="kkkkssss영진약품"
    st_name=st_name.decode('utf8')
    url="http://search.daum.net/search?w=news&nil_search=btn&enc=utf8&q=%ED%95%9C%EA%B5%AD%EA%B0%80%EC%8A%A4%EA%B3%B5%EC%82%AC&period=u&sd=20160810004425&ed=20160811010425&DA=STC&sort=1&cluster=n"
    coldate='2016-08-16'
    contents='빠르게 활용하는 파이썬 3.2 프로그래밍 – 14장 데이터베이스'
    contents=contents.decode('utf8')
    cursor.execute("insert into companyName(Name,url,coldate,contents) values(?,?,?,?);",(st_name,url,coldate,contents))

    con.commit()

    result = cursor.execute("select * from companyName")

    data = result.fetchall()
    print 'fetchall=', data

    for rows in cursor:
        print 'test=', rows

    con.close()

def mkdir():
    if not os.path.exists('html/except'):
        os.mkdir('html/except/')


def list_test():
    matchingstate='matching'
    contentstxt='asdfasdfsadfsafd'

    matchingYN=[]

    rlt={
                'matchingYN':matchingstate,
                'contents':contentstxt
                }

    rlt2={
                'matchingYN':'nonono',
                'contents':'kkkkkkkkkkkkk'
                }
    matchingYN.append(rlt)
    matchingYN.append(rlt2)

    print matchingYN

    print matchingYN[1]


def sql_isExists(cp_Name):
    con = sqlite3.connect("st_tmp.db")

    print(type(con))
    cursor = con.cursor()
    sqlStr="SELECT count(*) FROM companyName where Name='%s'"%(cp_Name)
    print sqlStr
    cur_rslt=cursor.execute(sqlStr)
    row=cur_rslt.fetchone()
    print row[0]
    cursor.close()
    con.close()

    if row==0:
        return 0
    else:
        return 1





sql_isExists('박만영')
#mkdir()

#list_test()
#open_ie()
#sqllite_insert(1,1,2,3)
# ariticle_save()
# load_tt()
#
# load_json()
#
# load_data()

# tt()
# load_tt()
